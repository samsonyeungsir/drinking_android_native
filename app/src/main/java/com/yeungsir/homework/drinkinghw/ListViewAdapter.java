package com.yeungsir.homework.drinkinghw;

import android.content.Context;
import android.text.NoCopySpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<milkteaclass>shoppingcartarr;

class ViewHolder{
    ImageView imageID;
   TextView teaname;
    TextView teaprice;


}

    public ListViewAdapter(Context context, ArrayList<milkteaclass>allteadata) {
        this.context = context;
        this.shoppingcartarr = allteadata;
    }

    @Override
    public int getCount() {
        return shoppingcartarr.size();
    }

    @Override
    public Object getItem(int position) {
        return shoppingcartarr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
   View itemView;
   ViewHolder viewHolder;
   milkteaclass objmilkteaclass = (milkteaclass)getItem(position);
        if ( convertView == null  ) {
            itemView = LayoutInflater.from(context).inflate(R.layout.teaitem,null);
            viewHolder = new ViewHolder();

            viewHolder.imageID = itemView.findViewById(R.id.imageView2);
            viewHolder.teaname = itemView.findViewById(R.id.teaname);
            viewHolder.teaprice= itemView.findViewById(R.id.price);

            itemView.setTag(viewHolder);
        }else {
itemView = convertView;
            viewHolder = (ViewHolder) convertView.getTag();

        }
viewHolder.imageID.setImageResource(objmilkteaclass.getImageID());
        viewHolder.teaname.setText(objmilkteaclass.getTeaname());
        viewHolder.teaprice.setText(""+objmilkteaclass.getTeaprice());
        return itemView;

   }
}
