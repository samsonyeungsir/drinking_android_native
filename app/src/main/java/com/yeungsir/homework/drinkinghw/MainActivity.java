package com.yeungsir.homework.drinkinghw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
milkteaclass tea1;
    milkteaclass tea2;
    milkteaclass tea3;
    milkteaclass tea4;
    milkteaclass tea5;
    milkteaclass tea6;

    static ArrayList<milkteaclass>mainmilktea;
    static ArrayList<milkteaclass>shoppingcarttea;
    ListViewAdapter myListViewAdapter;
    ListView myListview;
    Button shoppingcartbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_main);
mainmilktea = new ArrayList<>();
shoppingcarttea = new ArrayList<>();
        tea1 =new milkteaclass(R.drawable.tea1,"鮮奶茶" , 12.0,"","",0);
        tea2 =new milkteaclass(R.drawable.tea2,"烏龍奶茶" , 24.2,"","",0);
        tea3 =new milkteaclass(R.drawable.tea3,"黑鑚奶茶" , 36.0,"","",0);
        tea4 =new milkteaclass(R.drawable.tea4,"珍珠拿鐵" , 48.4,"","",0);
        tea5 =new milkteaclass(R.drawable.tea1,"綠抹茶" , 60.0,"","",0);
        tea6 = new milkteaclass(R.drawable.tea2,"鮮綠茶" , 72.8,"","",0);

        mainmilktea.add(tea1);
        mainmilktea.add(tea2);
        mainmilktea.add(tea3);
        mainmilktea.add(tea4);
        mainmilktea.add(tea5);
        mainmilktea.add(tea6);

     myListview = findViewById(R.id.mylistview);
       myListViewAdapter = new ListViewAdapter(this,mainmilktea);
       myListview.setAdapter(myListViewAdapter);

        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int index = position;

                Intent orderIntent = new Intent(MainActivity.this, orderActivity.class);

                orderIntent.putExtra("indexKey", index);
               startActivity(orderIntent);
            }
        });

 shoppingcartbutton= findViewById(R.id.cartbutton);
        shoppingcartbutton .setEnabled(false);
 shoppingcartbutton.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View v) {
         Intent shoppingintent = new Intent(MainActivity.this, shoppingcart.class);

         startActivity(shoppingintent);
     }

 });


    }

    @Override
    protected void onResume() {
        super.onResume();

        shoppingcartbutton.setEnabled(false);
        int number = shoppingcarttea.size();
        if(number != 0){
            shoppingcartbutton .setEnabled(true);

        }
        shoppingcartbutton.setText("購物車("+number+")");
   }
}