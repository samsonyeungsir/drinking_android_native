package com.yeungsir.homework.drinkinghw;

public class milkteaclass {
int imageID;
String teaname;
double teaprice;
String ice;
String milk;
int qty;

    public milkteaclass(int imageID, String teaname, double teaprice, String ice, String milk, int qty) {
        this.imageID = imageID;
        this.teaname = teaname;
        this.teaprice = teaprice;
        this.ice = ice;
        this.milk = milk;
        this.qty = qty;
    }

    public milkteaclass() { }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getTeaname() {
        return teaname;
    }

    public void setTeaname(String teaname) {
        this.teaname = teaname;
    }

    public double getTeaprice() {
        return teaprice;
    }

    public void setTeaprice(double teaprice) {
        this.teaprice = teaprice;
    }

    public String getIce() {
        return ice;
    }

    public void setIce(String ice) {
        this.ice = ice;
    }

    public String getMilk() {
        return milk;
    }

    public void setMilk(String milk) {
        this.milk = milk;
    }

    public int getqty() {
        return qty;
    }

    public void setqty(int milk) {
        this.qty = qty;
    }
}
