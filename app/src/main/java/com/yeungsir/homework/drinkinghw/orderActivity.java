package com.yeungsir.homework.drinkinghw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextClock;
import android.widget.TextView;

import java.util.ArrayList;

public class orderActivity extends AppCompatActivity {

Button enter;
Button add;
Button minus;
int qty;
   ImageView teaimg;
    TextView selectedteaname;
   TextView selectedprice;
   String Selectedmilk;
   String Selectedice;
    double selectedtotalprice;
    int itemcount=0 ;

    RadioButton noice;
    RadioButton nommralice;
    RadioButton moreice;
    RadioButton nomilk;
    RadioButton normalmilk;
    RadioButton moremilk;

    TextView total;
    int selectedID;
static ArrayList<milkteaclass>milkteacart;
milkteaclass objselectedmt;
milkteaclass objshoppingcart;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order2);
        milkteacart= new ArrayList<>();
        teaimg= findViewById(R.id.selectedimg);
        selectedteaname = findViewById(R.id.selectedname);
        selectedprice = findViewById(R.id.selecedprice);
        noice=findViewById(R.id.noiceButton);
        nommralice = findViewById(R.id.normaliceButton);
        moreice = findViewById(R.id.normaliceButton);
        nomilk = findViewById(R.id.nosweet);
        normalmilk = findViewById(R.id.normalsweet);
        moremilk = findViewById(R.id.moresweet);
        Selectedice = "正常冰";
        Selectedmilk = "正常奶";
Intent myIntent = getIntent();
selectedID = myIntent.getIntExtra("indexKey",0);

objselectedmt = MainActivity.mainmilktea.get(selectedID);

        teaimg.setImageResource(objselectedmt.getImageID());
        selectedteaname.setText(objselectedmt.getTeaname());
        selectedprice.setText(""+objselectedmt.getTeaprice());


add= findViewById(R.id.addbutton);
minus= findViewById(R.id.minbutton);
total = findViewById(R.id.displaytotal);

qty = 1;
total.setText(String.valueOf(qty));

add.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

    qty= qty+1;
        total.setText(String.valueOf(qty));
    }
});

minus.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
if(qty==1){
    qty=1;
}else{
    qty = qty - 1;
    total.setText(String.valueOf(qty));
}
    }
});


enter = findViewById(R.id.confirmbutton);
enter.setOnClickListener(new View.OnClickListener() {

    @Override
    public void onClick(View v) {
if(noice.isChecked()){
Selectedice = "走冰";
}else if (nommralice.isChecked()){
    Selectedice = "正常冰";
}else if(moreice.isChecked()){
    Selectedice = "真多冰";
}
        if(nomilk.isChecked()){
            Selectedmilk = "走奶";
        }else if (nommralice.isChecked()){
            Selectedmilk = "正常奶";
        }else if(moreice.isChecked()){
            Selectedmilk = "真多奶";
        }
selectedtotalprice = Double.valueOf(qty)* objselectedmt.getTeaprice();

        objshoppingcart= new milkteaclass(objselectedmt.getImageID(),objselectedmt.getTeaname(),selectedtotalprice,Selectedice,Selectedmilk,qty);
        MainActivity.shoppingcarttea.add(objshoppingcart);

        finish();
    }
});

    }


}