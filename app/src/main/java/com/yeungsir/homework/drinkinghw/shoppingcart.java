package com.yeungsir.homework.drinkinghw;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static com.yeungsir.homework.drinkinghw.MainActivity.shoppingcarttea;
import static com.yeungsir.homework.drinkinghw.R.layout.*;

public class shoppingcart extends AppCompatActivity {

    Button checkoutbutton;
    ListView shoppingitem;
    double subtotal;
    shoppingcartlistview shoppingcartAdapter;
TextView finaltotal;
double finalcalulation=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_shoppingcart);

        finaltotal= findViewById(R.id.finaltotal);
        shoppingitem = findViewById(R.id.shoppingcartlistview);
      shoppingcartAdapter = new shoppingcartlistview(this, MainActivity.shoppingcarttea);
       shoppingitem.setAdapter(shoppingcartAdapter);

        for ( milkteaclass obj:MainActivity.shoppingcarttea) {
            double strPrice = obj.getTeaprice();
           double strQuantity = Double.valueOf(obj.getqty());
        subtotal = strPrice * strQuantity;
           finalcalulation = subtotal + finalcalulation;
        }
        finalcalulation = subtotal + finalcalulation;

        finaltotal.setText("$"+finalcalulation);

        checkoutbutton= findViewById(R.id.cashoutbutton);
        checkoutbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(applicationContext,"" itemCount + " 已加入 "+" x ", Toast.LENGTH_LONG).show();
                Toast.makeText(shoppingcart.this,"成功下單",Toast.LENGTH_LONG).show();
                MainActivity.shoppingcarttea = new ArrayList<>();
                finish();
            }
        });

    }



}