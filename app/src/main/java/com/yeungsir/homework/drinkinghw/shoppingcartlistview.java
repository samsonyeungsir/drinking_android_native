package com.yeungsir.homework.drinkinghw;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class shoppingcartlistview extends BaseAdapter {

    private Context context;
    private ArrayList<milkteaclass> shoppingcartlist;

    class ViewHolder{
        ImageView imageID;
        TextView teaname;
        TextView price;
        TextView subtotal;
        TextView ice;
        TextView milk;

    }

    public shoppingcartlistview(Context context, ArrayList<milkteaclass> shoppingcarttea) {
        this.context = context;
        this.shoppingcartlist =shoppingcarttea;
    }

    @Override
    public int getCount() {
        return shoppingcartlist.size();
    }

    @Override
    public Object getItem(int position) {
        return shoppingcartlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View shoppingview;
        ViewHolder shoppingviewHolder;
        milkteaclass objshopping = (milkteaclass)getItem(position);
        if ( convertView == null  ) {
            shoppingview = LayoutInflater.from(context).inflate(R.layout.shoppingcart_item,null);
            shoppingviewHolder = new ViewHolder();

            shoppingviewHolder.imageID = shoppingview.findViewById(R.id.imageView);
            shoppingviewHolder.teaname = shoppingview.findViewById(R.id.shoppingitemname);
            shoppingviewHolder.price = shoppingview.findViewById(R.id.shoppingqty);
            shoppingviewHolder.subtotal= shoppingview.findViewById(R.id.shoppingsubtotal);
            shoppingviewHolder.milk= shoppingview.findViewById(R.id.shoppingmilk);
            shoppingviewHolder.ice= shoppingview.findViewById(R.id.shoppingice);
            shoppingview.setTag( shoppingviewHolder);
        }else{
            shoppingview = convertView;
            shoppingviewHolder = (ViewHolder) convertView.getTag();
        }
        shoppingviewHolder.imageID.setImageResource(objshopping.getImageID());
        shoppingviewHolder.teaname.setText(objshopping.getTeaname());
        shoppingviewHolder.subtotal.setText("$"+objshopping.getTeaprice());
        shoppingviewHolder.price.setText(""+objshopping.getqty());
        shoppingviewHolder.milk.setText(objshopping.getMilk());
        shoppingviewHolder.ice.setText(objshopping.getIce());

         return shoppingview;
    }
}
